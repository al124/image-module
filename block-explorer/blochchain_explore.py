import requests
import json
from utils import JSON
INDEX_NAME = "index.json"

class blockchain:
    def __init__(self,type="bitcoin",first = "000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f"):
        self.base_url = "https://blockchain.info"
        self.first = first
    def get_next(self,current):
        uri = "/rawblock/" + str(current)
        next = requests.get(self.base_url+uri)
        j = next.json()
        next_hash = j["next_block"][0]
        block_index = j["block_index"]
        tx = j["tx"]
        txs = []
        for t in tx:
            infos = dict()
            hash = "#" + str(t["hash"])[-6:]
            infos["color"] = hash
            infos["size"] = str(t["size"])
            txs.append(infos)
        return str(next_hash),int(block_index),txs
    def download_blocks(self,limit=1000):
        blocks = dict()
        info = dict()
        info["hash"] = self.first
        next,index,txs = self.get_next(self.first)
        name = str(index) 
        info["tx"] = txs
        info["next_block"] = next
        blocks[index] = info
        indexes = dict()
        for i in range(limit):
            info = dict()
            if i % 100 == 0:
                print("Blocchi eseguiti:",i)
            info["hash"] = next
            next_next,index,txs = self.get_next(next)     
            info["next_block"] = next_next
            info["tx"] = txs
            blocks[index] = info
            indexes[index] = next
            next = next_next
        name = name + "_" + str(index) + "_" + str(limit+1) + ".json"
        JSON().setJSON(name,blocks)
        last = self.set_index(indexes)
        return (index + 1 ,next)
    def set_index(self,indexes):
        old_index = JSON().getJSON(INDEX_NAME)
        index = dict()
        for k in old_index.keys():
            index[int(k)] = old_index[k]
        for k in indexes.items():
            if str(k[0]) not in index.keys():
                index[k[0]] = k[1]
        JSON().setJSON(INDEX_NAME,index)
        return None

#blockchain().download_blocks(1000)