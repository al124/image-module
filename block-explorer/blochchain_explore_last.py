import requests
import json
from utils import JSON
INDEX_NAME = "index.json"

class blockchain:
    def __init__(self,type="bitcoin"):
        self.base_url = "https://blockchain.info"
        self.last = self.get_last()
    def get_last(self):
        uri = "/latestblock"
        last = requests.get(self.base_url+uri)
        j = last.json()
        hash = j["hash"]
        return str(hash)
    def get_prev(self,current):
        uri = "/rawblock/" + str(current)
        prev = requests.get(self.base_url+uri)
        j = prev.json()
        prev_hash = j["prev_block"]
        block_index = j["block_index"]
        tx = j["tx"]
        txs = []
        for t in tx:
            infos = dict()
            hash = "#" + str(t["hash"])[-6:]
            infos["color"] = hash
            infos["size"] = str(t["size"])
            txs.append(infos)
        return str(prev_hash),str(block_index),txs
    def download_blocks(self,limit=1000):
        blocks = dict()
        info = dict()
        info["hash"] = self.last
        prev,index,txs = self.get_prev(self.last)
        name = "_" + index + "_" + str(limit) + ".json"
        info["prev_block"] = prev
        blocks[index] = info
        indexes = dict()
        for i in range(limit):
            if i % 100 == 0:
                print("Blocchi eseguiti:",i)
            info["hash"] = prev
            info = dict()
            prev_prev,index,txs = self.get_prev(prev)     
            info["prev_block"] = prev_prev
            info["tx"] = txs
            blocks[index] = info
            indexes[index] = prev
            prev = prev_prev
        name = index + name
        JSON().setJSON(name,blocks)
        self.set_index(indexes)
    def set_index(self,indexes):
        index = JSON().getJSON(INDEX_NAME)
        for k in indexes.items():
            if k[0] not in index.keys():
                index[k[0]] = k[1]
        JSON().setJSON(INDEX_NAME,index)

blockchain().download_blocks(1000)