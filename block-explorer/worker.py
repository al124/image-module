###### This worker repeat only is job #######
from utils import JSON
from blochchain_explore import blockchain

NEXT_TO_DO = "next_to_do.json"
NEW_FILES = 10
FILESIZE = 1000
def next_to_do(next_tuple):
        next = dict()
        next[next_tuple[0]] = next_tuple[1]
        JSON().setJSON(NEXT_TO_DO,next)
        return None

#USE ONLY FOR THE FIRST BLOCK
#next_to_do(blockchain().download_blocks(99))

#USE TO CONTINUE DATA EXTRACTION
first = [v for v in JSON().getJSON(NEXT_TO_DO).values()][0]
for i in range(NEW_FILES):
    next_to_do(blockchain(first=first).download_blocks(FILESIZE - 1))
    first = [v for v in JSON().getJSON(NEXT_TO_DO).values()][0]
