from image import image
from PIL import ImageColor
import random
SCALE_FACTOR = 2
MAX_SIZE = 512

def from_hex_to_color(hash):
    return "#" + str(hash)[-6:]
def draw_square(size,color):
    square = []
    pix = ImageColor.getcolor(color, "RGB")
    square_size = int(int(size)/SCALE_FACTOR) 
    if square_size >= MAX_SIZE:
        square_size = MAX_SIZE - 1
    dim_square = square_size 
    for i in range(dim_square):
        r = []
        for j in range(dim_square):
            r.append(pix)
        square.append(r)
    return square
def main(filename,w,h,txs=[],mod=255,background="#ffffff"):
    background = ImageColor.getcolor(background, "RGB")
    squares = []
    for s in txs:
        square = draw_square(s["size"],s["color"])
        squares.append(square)
    arr = []
    for i in range(h):
        row = []
        for j in range(w):
            pix = (-1,-1,-1)
            row.append(pix)
        arr.append(row)
    #for i in range(h):
    #    row = []
    #    for j in range(w):
    #        pix = background
    #        row.append(pix)
    #    arr.append(row)
    for s in squares:
        start_w = random.randint(0,w)
        start_h = random.randint(0,h)
        s_i = 0
        s_j = 0
        l = len(s)
        for i in range(start_h,h):
            row = []
            for j in range(start_w,w):
                if s_i < l:
                    pix = s[s_i][s_j]
                    pix_old = arr[i][j]
                    if pix_old == (-1,-1,-1):
                        rs = pix[0]
                        gs = pix[1]
                        bs = pix[2]
                    else:
                        rs = (pix[0] + pix_old[0]) %mod
                        gs = (pix[1] + pix_old[1]) %mod
                        bs = (pix[2] + pix_old[2]) %mod
                    arr[i][j] = (rs,gs,bs)
                    s_i = s_i +1
            s_i = 0
            if s_j < l-1:
                s_j = s_j +1
            else:
                break
    for i in range(h):
        for j in range(w):
            pix = arr[i][j]
            if pix == (-1,-1,-1):
                arr[i][j] = background            
    img = image(img_arr=arr)
    img.save_it(filename)



from utils import JSON
from os import listdir
from os import mkdir
from os.path import isfile, join
#USE ONLY FOR THE FIRST BLOCK
#next_to_do(blockchain().download_blocks(99))

#USE TO CONTINUE DATA EXTRACTION
WORKDIR = "./0-9999/"


jfiles = [f for f in listdir(WORKDIR) if isfile(join("./0-9999/", f))]
for jfile in jfiles:
    blocks = JSON().getJSON(WORKDIR + jfile)
    dir = jfile.replace("_1000.json","")
    mkdir("./NFT/10k/"+dir)
    for k in blocks.keys():
        background = from_hex_to_color(blocks[k]["hash"])
        txs = blocks[k]["tx"]
        main("./NFT/10k/"+dir+"/"+str(k)+".png",512,512,txs=txs,mod = 255,background=background)