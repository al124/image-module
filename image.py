from utils import handler

class image:
    def __init__(self,width=0,height=0,img_arr=[]):
        self.w = width
        self.h = height
        self.img_arr = []
        if img_arr != []:
            self.set_img_arr(img_arr)
    def get_h(self):
        return self.h
    def get_w(self):
        return self.w
    def get_img_arr(self):
        return self.img_arr
    def is_arr_img(self,arr):
        response  = True
        w_arr = len(arr[0])
        for row in arr:
            #larghezza immagine differente
            if len(row) != w_arr:
                response = False
            else:
                s_row = set(row)
                for pixel in row:
                    r = pixel[0]
                    g = pixel[1]
                    b = pixel[2]
                    #formato pixel errato
                    if type(pixel) == tuple and len(pixel)==3 and 0<=r<=255 and 0<=g<=255 and 0<=b<=255:
                        continue
                    else:
                        response = False
            if not response:
                break
        return response
    def set_img_arr(self,arr=[]):
        try:
            if self.is_arr_img(arr):
                self.img_arr = arr
                self.h = len(arr)
                self.w = len(arr)
            else:
                pass
            return 0
        except:
            return -1
    def modular_sum(self,a,b):
        s = (a + b) % 255
        return s
    def save_it(self,filename):
        handler().save_image(self.img_arr,filename)


