from image import image
import random

#Salva immagine casuale
def save_random(filename,w,h):
    arr = []
    for i in range(h):
        row = []
        for j in range(w):
            r = random.randint(0,255)
            g = random.randint(0,255)
            b = random.randint(0,255)
            pix = (r,g,b) 
            row.append(pix)
        arr.append(row)
    #print(arr)
    img = image(img_arr=arr)
    img.save_it(filename)
#save_random("random.png",1024,720)

#Salva immagine casuale
def save_random_scale(filename,w,h):
    arr = []
    r = random.randint(0,255)
    g = random.randint(0,255)
    b = random.randint(0,255)
    row = []
    for j in range(w):
        if r >= 255:
            r = random.randint(0,255)
            g = random.randint(0,255)
            b = random.randint(0,255)
        else:
            r = r + 1
        pix = (r,g,b) 
        row.append(pix)
    for i in range(h):
        arr.append(row)
    #print(arr)
    img = image(img_arr=arr)
    img.save_it(filename)
#save_random_scale("random.png",1024,720)
def random_square(filename,w,h,num_squares=1):
    squares = []
    for n in range(num_squares):
        square = []
        r = random.randint(0,255)
        g = random.randint(0,255)
        b = random.randint(0,255)
        pix = (r,g,b) 
        dim_square = random.randint(0,int(512/2))
        for i in range(dim_square):
            r = []
            for j in range(dim_square):
                r.append(pix)
            square.append(r)
        squares.append(square)
    arr = []
    for i in range(h):
        row = []
        for j in range(w):
            pix = (255,255,255) 
            row.append(pix)
        arr.append(row)
    for s in squares:
        start_w = random.randint(0,w)
        start_h = random.randint(0,h)
        s_i = 0
        s_j = 0
        l = len(s)
        for i in range(start_h,h):
            row = []
            for j in range(start_w,w):
                if s_i < l:
                    pix = s[s_i][s_j]
                    arr[i][j] = pix
                    s_i = s_i +1
            s_i = 0
            if s_j < l-1:
                s_j = s_j +1
            else:
                break
    #print(arr)
    img = image(img_arr=arr)
    img.save_it(filename)
#random_square("test.png",512,512,100)
def random_square2(filename,w,h,num_squares=1,darkness=0,mod=255):
    squares = []
    for n in range(num_squares):
        square = []
        r = random.randint(0,255)
        g = random.randint(0,255)
        b = random.randint(0,255)
        pix = (r,g,b) 
        dim_square = random.randint(0,int(512/2))
        for i in range(dim_square):
            r = []
            for j in range(dim_square):
                r.append(pix)
            square.append(r)
        squares.append(square)
    arr = []
    for i in range(h):
        row = []
        for j in range(w):
            pix = (255,255,255) 
            row.append(pix)
        arr.append(row)
    for s in squares:
        start_w = random.randint(0,w)
        start_h = random.randint(0,h)
        s_i = 0
        s_j = 0
        l = len(s)
        for i in range(start_h,h):
            row = []
            for j in range(start_w,w):
                if s_i < l:
                    pix = s[s_i][s_j]
                    pix_old = arr[i][j]
                    rs = (pix[0] + pix_old[0]) %mod
                    gs = (pix[1] + pix_old[1]) %mod
                    bs = (pix[2] + pix_old[2]) %mod
                    arr[i][j] = (rs,gs,bs)
                    s_i = s_i +1
            s_i = 0
            if s_j < l-1:
                s_j = s_j +1
            else:
                break
    #print(arr)
    img = image(img_arr=arr)
    img.save_it(filename)
random_square2("test2.png",512,512,1000,mod = 255)